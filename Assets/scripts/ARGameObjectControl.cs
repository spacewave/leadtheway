﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ARGameObjectControl : MonoBehaviour {
	
	public static ARGameObjectControl instance;

	// Initialization of Game control
	void Awake () 
	{
		if (instance == null) {
			instance = this;
		} else if (instance != null) {
			Destroy (gameObject);
		}
	}
		
	// Will put trackable in relation to the active playboard
	public void PlaceOnPlayBoard(Transform trackableTrans, PlayingBoard activePlayBoard, GameObject directionTrackablePrefab = null) 
	{
		GameObject trackableObject = trackableTrans.gameObject;
		FocusButtonInteraction interactionComponent = trackableObject.GetComponentInChildren<FocusButtonInteraction> ();
		trackableObject.transform.GetChild(0).SetParent (activePlayBoard.transform);

		// If the trackable is a direction arrow a new model will be instantiated onto the trackable
		if (trackableTrans.name.Contains ("directionArrow_imageTarget")) 
		{
			GameObject newModel = Instantiate (directionTrackablePrefab,
				trackableTrans.position + directionTrackablePrefab.transform.position,
				trackableTrans.rotation * directionTrackablePrefab.transform.rotation,
				trackableTrans);
			
			newModel.transform.localScale = directionTrackablePrefab.transform.localScale;
		}
		if (interactionComponent != null) {
			//Destroy (interactionComponent);
			interactionComponent.Reset ();
			interactionComponent.outcome = ButtonInteractionOutcome.TAKE_BACK;
			interactionComponent.thresholdForInteraction = 10f;
		}
	}

	// Will set the gameObject back to being a child of the trackableObject
	public void unPlaceOnPlayBoard(GameObject gameObject, Transform trackableTrans, GameObject shipPrefab = null)
	{
		Destroy (gameObject);

		if (gameObject.name.Contains ("ship_model")) {
			GameObject newModel = Instantiate (shipPrefab,
				trackableTrans.position + shipPrefab.transform.position,
				trackableTrans.rotation * shipPrefab.transform.rotation,
				                      trackableTrans);
			newModel.transform.localScale = shipPrefab.transform.localScale;
		}
	}
}
