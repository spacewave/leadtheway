﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingBoard : MonoBehaviour {

	private Level currentLevel;					// The level currently Loaded on the playing board
	public Level CurrentLevel {
		get{ return currentLevel; }
	}
	private Level upcomingLevel;				// The Level, which will be played next up
	private int levelsCompleted;
	public int LevelsCompleted{
		get{return levelsCompleted;}
	}
	public LevelGenerationStrategy genStrat;	// After which strategy the levels on the playboard are generated
	public GameObject goalPrefab;
	public GameObject[] obstaclePrefabs;
	public GameObject airshipPrefab;
	public GameObject directionArrowPrefab;
	private List<GameObject> obstacleObjects;
	public List<GameObject> ObstacleObjects {
		get { return obstacleObjects; }
	}
	private GameObject goalObject;
	public GameObject GoalObject {
		get { return goalObject; }
	}
	private GameObject flyingObject;

	void Start() {
		levelsCompleted = 0;
		obstacleObjects = new List<GameObject> ();
		genStrat = new RandomGeneration (this);
		flyingObject = GameObject.Find ("ship_model");
		currentLevel = GameControl.instance.CreateNextLevel (levelsCompleted, this, flyingObject);
		InstantiateLevel ();
		Debug.Log (currentLevel);
	}
		
	public void ReInstGoal(){
		Destroy (goalObject);
		InstantiateGoal ();
	}

	public void ReInstObstacles(){
		foreach (GameObject obstacle in obstacleObjects) {
			Destroy (obstacle);
		}
		InstantiateObstacles ();
	}

	// This Method will load up the Level and visualize it 
	public void LoadNextLevel(Level level) {
		upcomingLevel = level;
	}

	// This Method set the current Level up
	public void NextLevel(){
		Debug.Log(levelsCompleted);
		levelsCompleted += 1;
		LoadNextLevel(GameControl.instance.CreateNextLevel  (levelsCompleted + 1, this, flyingObject));
		GameControl.instance.DestroyCurrentLevelGameObjects (this);
		currentLevel = upcomingLevel;
		InstantiateLevel ();
	}

	// Instantiating Level will call the Instantiate Goal and Obstacles Method
	private void InstantiateLevel (){
		InstantiateGoal ();
		InstantiateObstacles();
	}

	// Will generate a goal position according to the strategy on the board and then instantiate it as a gameObject in the scene 
	private void InstantiateGoal() {
		currentLevel.Goal = genStrat.CalculateGoalForLevel (currentLevel, flyingObject.transform);
		goalObject = Instantiate (goalPrefab, currentLevel.Goal, Quaternion.identity, transform) as GameObject;
	}

	// Will generate the obstacle in the scene according to the strategy on the board and then instatiate the obstacles in the scene
	private void InstantiateObstacles() {
		currentLevel.Obstacles = genStrat.CalculateObstaclesForLevel (currentLevel);
		System.Char delimiter = ':';
		foreach (KeyValuePair<string, Vector3> obstacle in currentLevel.Obstacles) {
			foreach (GameObject prefab in obstaclePrefabs) {
				if (prefab.name.Equals (obstacle.Key.Split(delimiter)[1])) {
					obstacleObjects.Add (Instantiate (prefab, obstacle.Value, Quaternion.identity, transform) as GameObject);
				}
			}
		}
	}
		
}
