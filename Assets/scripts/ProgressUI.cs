﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// control the UI Overlay; not used for now
/// </summary>

public class ProgressUI : MonoBehaviour
{
    public Text progressText;

    public ButtonInteractionOutcome outcome;

    private GameObject rayView;
    private float progressTimer;


    public void setText(string progress)
    {
        switch (outcome)
        {
            case ButtonInteractionOutcome.START:
                progressText.text = "Place Ship";
                break;
            case ButtonInteractionOutcome.PLACE_SHIP:
                 progressText.text = "Place DirectionArrow";
                break;
        }
    }
}
