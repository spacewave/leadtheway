﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using MonsterLove.StateMachine;

public class GameControl  : MonoBehaviour {

#region public Variables
    public static GameControl instance;
	public GameObject playFieldPrefab;
	public GameObject shipPrefab;
	public GameObject directionArrowPrefab;
    public ButtonInteractionOutcome outcome;
#endregion


#region private Variables

    private GameObject menuCanvasOBJ;       //menu UI
    private GameObject overlayCanvasOBJ;    //ingame UI
    private StateMachine<ButtonInteractionOutcome> fsm;     //finite state machine 

#endregion




    // Initialization of Game control
    void Awake () {
        //Initialize the state machine and start game logic
        fsm = StateMachine<ButtonInteractionOutcome>.Initialize(this, ButtonInteractionOutcome.START);

        if (instance == null) {
			instance = this;
		} else if (instance != null) {
			Destroy (gameObject);
		}
	}
		
	void Start(){
		menuCanvasOBJ = GameObject.Find ("menu_canvas");
	}

    
    
    
    
#region methods

    // Get from Menu Screen into the actual AR and start looking for ImageCodes, the PlayBoard preferably
    public void StartAR(){
		GameObject menuPanel = menuCanvasOBJ.transform.GetChild (0).gameObject;
		menuPanel.SetActive (false);
		//Instantiate (playFieldPrefab);
		//Instantiate (shipPrefab);
		//Instantiate (directionArrowPrefab);
	}
		
	// Will start the Game of a specific playBoard
	public void StartGame(PlayingBoard playBoard){
		playBoard.enabled = true;
	}

	// Empty Cache of current Game and restart the session
	void RestartGame(){
		
	}

	// If the FO reached the Destination, the next level on the playingBoard will be loaded
	public Level CreateNextLevel(int levelNumber, PlayingBoard activePlayBoard, GameObject flyingObject){
		Level level = new Level (activePlayBoard, levelNumber, flyingObject.transform);
		return level;
	}

	// Destroy the Objects currently loaded in the Level
	public void DestroyCurrentLevelGameObjects(PlayingBoard activePlayBoard) {
		foreach (GameObject obstacle in activePlayBoard.ObstacleObjects) {
			Destroy (obstacle);
		}
		Destroy (activePlayBoard.GoalObject);
	}
		
	// Will make the FlyingObject start its engine and fly away to far lands, .....
	public void TakeOff (GameObject flyingObject){
		flyingObject.GetComponent<ShipBehaviour> ().thrust = 5;
		flyingObject.GetComponentInChildren<ParticleSystem> ().Play ();
	}

    #endregion

    ///<summary>
    /// controls the gamelogic and defines the active state
    /// </summary>
    #region stateMachine logic

    #region START
    private void START_Enter()
    {
        print("game has started");
        //Init the game and menu here
    }

    private void START_Update()
    {
        //check if conditions are met to go to the next state: Generate goal
        // fsm.ChangeState(States.GENERATE_GOAL);
    }
    #endregion

    #region GENERATE_GOAL

    #endregion

    #region GENERATE_OBSTACLES

    #endregion

    #region REACHED_DESTINATION

    #endregion

    #region TAKE_OFF

    #endregion

    #region PLACE_SHIP

    #endregion

    #region PLACE_ARROW

    #endregion
    #endregion
}
