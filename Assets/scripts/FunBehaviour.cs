﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Add this script to the airship to have some fun :D
public class FunBehaviour : MonoBehaviour {

    public GameObject funPrefab;
    public float spawningRate = 5f;

    private void Start () {
        StartCoroutine ("SpawnEveryTenSeconds");
    }

    // Challenge, try tweaking the values of the directionArrow prefab, so it will become faster and faster infinately :D
    IEnumerator SpawnEveryTenSeconds () {
        while (true) {
            Vector3 spawnPosition = new Vector3 ((transform.position-transform.forward).x, 
                (transform.position - transform.forward).y, (transform.position - transform.forward).z);
            Vector3 spawnRotation = new Vector3(0f, transform.eulerAngles.y+65f, 0f);
            Instantiate (funPrefab, spawnPosition, Quaternion.Euler(spawnRotation));
            yield return new WaitForSeconds (spawningRate);
        }
    }
}
