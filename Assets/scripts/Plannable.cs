﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Plannable{

	// Navigation of an Object in a three dimensional coordinate system must be dependant to a route that has been planed mostly beforehand
	List <Vector3> PlanRoute ();	
	// Movement is necessary to progress and sometime reach the destination
	void MoveAccordingToPlan();				
	// Following the planned Route in the context to which it is applied
	void FollowPlannedRoute();		
	// Reachnig the Destination and do whatever is necessary
	void ReachedDestination();		
}
