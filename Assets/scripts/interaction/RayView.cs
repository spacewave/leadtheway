﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// A Ray is casted from the central viewpoint of the camera and thereby on kind of interaction is measured
public class RayView: MonoBehaviour {

	public GameObject rayViewFeedbackPrefab;
    public Transform progressBar;
    public float viewFeedBackRepetition;
	public float timer = 0f;
    private float threshold;                
	private ViewInteract lastInteract;

  /*  void Awake()
    {
        
        threshold = GameObject.FindGameObjectWithTag("ar_code").GetComponentInChildren<FocusButtonInteraction>().thresholdForInteraction;
    }*/
	// Update is called once per frame
	void Update () {

		// The RaycastHit Object the view Ray will potentially collide with
		RaycastHit hit;
		// The view Ray, which is casted from the transform origing forwards of the Object
		Ray viewRay = new Ray (transform.position, transform.forward);

		if (Physics.Raycast (viewRay, out hit)) {

			// This condition will reset the lastInteract, if 
			if (lastInteract != null && 
				(hit.collider.gameObject.GetComponent<ViewInteract>() == null || 
					lastInteract != hit.collider.gameObject.GetComponent<ViewInteract> ())) {
				lastInteract.Reset ();
				lastInteract = null;
			}

			// Intantiate the RayFeedback Object viewFeedBackRepetition frequency
			//var viewFeedBack = Instantiate (rayViewFeedbackPrefab, hit.point , 
				//Quaternion.LookRotation(new Vector3(hit.normal.x, 90, hit.normal.z))) as GameObject;
			timer += Time.deltaTime;
            //set the value of the progressBar in respect of timer
		    progressBar.GetComponent<Image>().fillAmount = Mathf.Clamp(timer/3, 0f, 0.75f); //timer/threshold
        
            //Destroy (viewFeedBack, 1/viewFeedBackRepetition);

			// If the the gameObject of the hit Collider has a ViewInteract Component set it up as last Interact and 
			// access its TimedInteract Method otherwise reset the timer
			if (hit.collider.gameObject.GetComponent<ViewInteract> () != null) {
				lastInteract = hit.collider.gameObject.GetComponent<ViewInteract> ();
				timer = hit.collider.gameObject.GetComponent<ViewInteract> ().TimedInteract (timer, hit) ? 0 : timer; //?: condition operator that returns 0 if true or timer if false
			} else {
				timer = 0f;
			    progressBar.GetComponent<Image>().fillAmount = 0;
            }

			// If the Ray does not hit anything reset the timer and set up the last interact as null
		}  else {
			lastInteract = null;
			timer = 0f;
		    progressBar.GetComponent<Image>().fillAmount = 0;
        }
	}

	public void resetTimer(){
		timer = 0f;
	}
}
