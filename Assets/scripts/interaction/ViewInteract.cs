﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ViewInteract {
	
	// Interaction, that will occur after the interaction boundary has been reached
	bool TimedInteract(float lookAtTime, RaycastHit hit);
	// Reset the interactable, so it can be interacted with, from a fresh start
	void Reset();
}
