﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonInteractionOutcome {
	START,
	GENERATE_GOAL,
	GENERATE_OBSTACLES,
	REACHED_DESTINATION,
	TAKE_OFF,
	PLACE_SHIP,
	PLACE_ARROW,
	TAKE_BACK,
    START_SHIP
}

public enum GameVariations {
	PROCEDURAL_GENERATED_RANDOM,
	PROCEDURAL_GENERATED_BALANCED,
	DESIGNED
}