﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehaviour : MonoBehaviour, Plannable {

    public float thrust;	
    public Vector3 dir;

    
    private Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponentInChildren<Rigidbody>();
        rb.AddForce(-transform.forward * thrust);

    }
	
	// Update is called once per frame
	void Update () {
		dir = GetComponentInChildren<Rigidbody>().velocity;
        //transform.rotation = Quaternion.LookRotation(-dir);
    }
		
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
        {
            Destroy(gameObject);
        }
    }

	// In order to reach the goal, a plan has to be prepared. 
	// Right now a the direction into, which the ship is fly is determined by a list of threedimensional vectors
	public List<Vector3> PlanRoute(){

		Debug.Log ("PlanningPath");
		List<Vector3> plan = new List<Vector3>();
		return plan;
	}

    public void TakeOff(float thrust)
    {
        rb.AddForce(-transform.forward * thrust);
    }

	// Once the Ship starts moving its PositionData will change and influence on the plan is minimal
	public void MoveAccordingToPlan (){
		Debug.Log ("Start Moving");
		transform.Translate (transform.forward * Time.deltaTime * thrust);
	}

	// To follow the plan the ship has to follow the plan from the order the operations are set 
	public void FollowPlannedRoute(){
		Debug.Log ("Is Following Route");
	}

	// Whether the Ship reaches the destination or not depends if the Barrier Object disturbed the route of the ship
	public void ReachedDestination(){
		Debug.Log ("Reached Destination");
	}
}
