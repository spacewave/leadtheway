﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pull : MonoBehaviour, IObstacle
{
    public float pull_radius = 100; // Radius to pull
    public float intensity = 1000000; // intensity of property force
    public float min_radius = 2; // Minimum distance to pull from
    public float distance_multiplier = 10; // Factor by which the distance affects force

    public LayerMask layers_to_pull = 5;
    //Interface member has to be implemented
	public IObstacle Obstacle()
    {
		return this;
    }

    // Function that runs on every physics frame
    void FixedUpdate()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, pull_radius, layers_to_pull);

        foreach (var collider in colliders)
        {
            
            Rigidbody rb = collider.GetComponent<Rigidbody>();

            if (rb == null) continue; // Can only pull objects with Rigidbody
          
            Vector3 direction = transform.position - collider.transform.position;

            if (direction.magnitude < min_radius) continue;

            float distance = direction.sqrMagnitude * distance_multiplier + 1; // The distance formula
           
            // Object mass also affects the gravitational pull
			rb.AddForce(direction.normalized * (intensity / distance) * rb.mass * Time.fixedDeltaTime);
        }
    }

}

