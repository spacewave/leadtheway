﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectionBehaviour : MonoBehaviour {

    public float movementForce = 10f;
    public float turningRate = 1f;
    public float pushRate = 1f;
    private Vector3 incomeVelocity;
    private RaycastHit centralRayHit;
    private Ray centralRay;
    private float timeInsideTrigger;
    private float trend = 0f;
    public string tagForRedirection;

    private bool reachedMiddleOfCollider = false;
    private bool closestIntersectReached = false;


    /// <summary>
    /// When the something will enter the Trigger Zone of the RedirectionArrow, the income Velocity will be saved one into the incomeVelocity Variable,
    /// also a central Ray from one end of the collission Bound to the other will be drawn. Also the time of the other Collider will be approximated
    /// </summary>
    /// <param name="other"> The Collider that entered the Trigger this script is attached to</param>
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("" + other.name + " entered Collider of " + gameObject.name);
        incomeVelocity = other.GetComponent<Rigidbody>().velocity;
        centralRay = drawCentralRay(GetComponent<Collider>().bounds);

        timeInsideTrigger = approxTimeInsideCollider(
            other.GetComponent<Rigidbody>().velocity.magnitude,
            other.GetComponent<Rigidbody>().velocity.normalized,
            other.transform.position, 
            CalculateExitPointOfCollider(GetComponent<Collider>().bounds),
            transform.position);
    }
    
    /// <summary>
    /// In order for the GameObject, the other collider is attached to, there need to be several checks,
    /// The GameObject need the right tag to be redirected, 
    ///     When a colission with the central Ray takes place,
    ///         there will also be a check, whether the hit is at optimalPosition 
    ///             for a redirect
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay (Collider other) {

        if (other.tag.Equals(tagForRedirection))
        {
            centralRayHit = CheckRayCastHit(centralRay);
            if (centralRayHit.collider != null)
            {
                Debug.Log(other.name + " has reached middle of Collider");
                closestIntersectReached = ColliderReachedOptimum(centralRayHit);
                if (closestIntersectReached)
                {
                    Debug.Log("Redirect");
                    Redirect(other.GetComponent<Rigidbody>());
                }
            }
        }
       // other.transform.position = Vector3.Lerp(other.transform.position, calculateExitPointOfCollider(GetComponent<Collider>(), transform), timeInsideTrigger * Time.deltaTime);
       // other.transform.rotation = Quaternion.Lerp(other.transform.rotation, gameObject.transform.rotation, timeInsideTrigger * Time.deltaTime);
        Debug.Log(centralRayHit.collider);

    }

    /// <summary>
    /// Will redirect the rigidbody into the direction the transform this script is attached to is heading, with the velocity the collider has entered the trigger
    /// </summary>
    /// <param name="rigidbody">The rigidbody, that will be redirected</param>
    private void Redirect(Rigidbody rigidbody)
    {
        if (rigidbody != null)
        {
            rigidbody.velocity = (-transform.forward * movementForce).normalized * incomeVelocity.magnitude;
            rigidbody.rotation = transform.rotation;
        }
    }

    /// <summary>
    /// Another Method to redirect the behaviour, by pushing the RigidBody attached to the GameObject, which has the other collider.
    /// This Method requires lots of tweaking, for the right redirect
    /// </summary>
    /// <param name="other">The collider, that is attached to the gameObject, that will be pushed</param>
    private void pushIncoming(Collider other)
    {

        //Will rotate the Object colliding with Redirector toward its own rotation, gradually
        other.transform.rotation = Quaternion.Lerp(other.transform.rotation, gameObject.transform.rotation, turningRate * Time.deltaTime);

        //Will push the Object towards the pointing direction, gradually
        Debug.Log("Adding force to Rigidbody of: " + other.name + "\nwith rate: "
            + Vector3.Lerp(Vector3.zero, (-transform.forward * movementForce), movementForce));
        other.GetComponent<Rigidbody>().AddForce(Vector3.Lerp(Vector3.zero, (-transform.forward * movementForce), pushRate * Time.deltaTime));
    }

    /// <summary>
    /// This Method calculates the exit point any incoming Object would be send to, by adding the bounds.extents to the bounds.center to the GameObject this 
    /// script is attached to
    /// </summary>
    /// <param name="bounds">The bounds are important for callibrating the length and exit point of the redirection arrow</param>
    /// <returns></returns>
    private Vector3 CalculateExitPointOfCollider(Bounds bounds)
    {
        Vector3 offset = new Vector3(0f, 0f, bounds.extents.z);
        offset = transform.rotation * offset;
        Vector3 inverseExitPoint = bounds.center + offset;
        Vector3 exitPointOfCollider = inverseExitPoint - transform.forward * bounds.size.z;

        return exitPointOfCollider ;
    }

    /// <summary>
    /// Will draw the Ray, by calculating a rayOrigin, which is opposed to the exit of the redirection arrow, the direction is forward, so towards the exit
    /// </summary>
    /// <param name="bounds">The bounds are important for callibrating the length and exit point of the redirection arrow</param>
    /// <returns></returns>
    private Ray drawCentralRay(Bounds bounds)
    {
        Vector3 rayOrigin = CalculateExitPointOfCollider(bounds) + transform.forward * bounds.size.z;
        Ray ray = new Ray(rayOrigin, -transform.forward);
        return ray;
    }

    /// <summary>
    /// Will check if the ray hits anything on layer 8, which is the playerLayer
    /// </summary>
    /// <param name="ray">The base for the Raycast hit</param>
    /// <returns></returns>
    private RaycastHit CheckRayCastHit(Ray ray)
    {
        RaycastHit hit = default(RaycastHit);
        Debug.DrawRay(ray.origin, -transform.forward * GetComponent<Collider>().bounds.size.z, Color.red);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity , 1<<8))
        {
            Debug.Log("Raycast Hit with:" + hit.collider.name);
            return hit;
        }

        return hit;
    }

    /// <summary>
    /// The ColliderReachedOptimim methods returns a boolean, when the optimum has been calcluated. It is calculated with differentials and trends 
    /// of the differentials. Right now the first local Minimum found will return a true boolean
    /// </summary>
    /// <param name="hit">The RaycastHit, that gives the information about the interection points</param>
    /// <returns></returns>
    private bool ColliderReachedOptimum(RaycastHit hit)
    {
        Ray ray = new Ray(centralRay.origin, hit.transform.position - centralRay.origin );
        Debug.DrawRay(centralRay.origin, hit.transform.position - centralRay.origin , Color.blue);

        RaycastHit comparisonHit;

        if(Physics.Raycast(ray, out comparisonHit, Mathf.Infinity, 1 << 8) ){
            if (Mathf.Abs(trend) - Mathf.Abs(comparisonHit.point.magnitude - hit.point.magnitude)< 0f && trend != 0f)
            {
                Debug.Log("Trend has changed, locale Minimum found" + (comparisonHit.point.magnitude - hit.point.magnitude));
                return true;
            }
            else trend = Mathf.Abs(comparisonHit.point.magnitude - hit.point.magnitude);
        }

        Debug.Log((comparisonHit.point.magnitude - hit.point.magnitude));
        return false;
    }

    /// <summary>
    /// Will calculte the time the incoming Object will stay in the collider
    /// </summary>
    /// <param name="velocity">The velocity of the incoming rigidbody</param>
    /// <param name="incomeDirection">The direction of the incoming rigidbody</param>
    /// <param name="entryPosition">The position of entry of the rigidbody entered</param>
    /// <param name="exitPosition">The exit position the rigidbody shall be directed to</param>
    /// <param name="middlePosition">the middle position is the center of the redirection arrow</param>
    /// <returns></returns>
    private float approxTimeInsideCollider(float velocity, Vector3 incomeDirection,Vector3 entryPosition, Vector3 exitPosition, Vector3 middlePosition)
    {
        float timeToMiddle = (middlePosition - entryPosition).magnitude / (incomeDirection * velocity).magnitude;
        float timeFromMiddleToExit = (exitPosition - middlePosition).magnitude / (incomeDirection * velocity).magnitude * velocity;
        float timeInsiderCollider = timeToMiddle + timeFromMiddleToExit;

        Debug.Log("[DEBUG] Incoming GameObject will spend: " + timeInsiderCollider + " time units inside Colider");
        return timeInsiderCollider;
    }
}
